import hxd.Key;
import h3d.mat.Texture;
import h2d.Bitmap;
import h2d.Tile;
import hxd.Pixels;

class Main extends hxd.App {
    static var COLOR_WHITE: Int = 0xFFFFFFFF;
    static var COLOR_BLACK: Int = 0xFF000000;
    static var SPF: Int = 128;

    var texture: Texture;
    var field: Field;

    override function init() {
        super.init();
        // Its actually ARGB Mode
        texture = new Texture(s2d.width, s2d.height, [Dynamic], RGBA);
        var pixels = Pixels.alloc(texture.width, texture.height, RGBA);
        for (y in 0...pixels.height) {
            for (x in 0...pixels.width) {
                pixels.setPixel(x, y, COLOR_WHITE);
            }
        }

        texture.uploadPixels(pixels);
        var tile = Tile.fromTexture(texture);
        var bmap = new Bitmap(tile, s2d);

        field = new Field(pixels, new Ant(Math.floor(pixels.width * 0.5), Math.floor(pixels.height * 0.5)));
        hxd.Window.getInstance().addEventTarget(onKey);
    }

    function onKey(event : hxd.Event) {
        switch (event.kind) {
            case EKeyUp: {
                switch (event.keyCode) {
                    case Key.NUMPAD_ADD, Key.D: {
                        Main.SPF *= 2;
                        trace('Increased to ${Main.SPF} per Frame');
                    }
                    case Key.NUMPAD_SUB, Key.I: {
                        if (Main.SPF * 0.5 < 1) {
                            Main.SPF = 1;
                        } else {
                            Main.SPF = Math.floor(Main.SPF * 0.5);
                        }
                        trace('Decreased to ${Main.SPF} per Frame');
                    }
                    default:
                }
            }
            default: 
        }
    }

    override function update(dt: Float) {

        for (x in 0...Main.SPF) {
            field.move();
        }

        texture.uploadPixels(field.px);
    }

    static function main() {
        new Main();
    }
}