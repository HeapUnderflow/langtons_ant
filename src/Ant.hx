import hxd.Direction;

class Ant {
    public var x : Int;
    public var y : Int;
    public var direction : Direction;
    
    public function new(x: Int, y: Int) {
        this.x = x;
        this.y = y;
        this.direction = Direction.Up;
    }

    public function move(field_set: Bool) {
        var nextDirection: Direction = direction;

        if (field_set) {
            switch (this.direction) {
                case Direction.Up:    nextDirection = Direction.Right;
                case Direction.Right: nextDirection = Direction.Down;
                case Direction.Down:  nextDirection = Direction.Left; 
                case Direction.Left:  nextDirection = Direction.Up;
            }
        } else {
            switch (this.direction) {
                case Direction.Up:    nextDirection = Direction.Left;
                case Direction.Right: nextDirection = Direction.Up;
                case Direction.Down:  nextDirection = Direction.Right; 
                case Direction.Left:  nextDirection = Direction.Down;
            }
        }

        switch (nextDirection) {
            case Direction.Up:    this.y += 1;
            case Direction.Right: this.x += 1;
            case Direction.Down:  this.y -= 1;
            case Direction.Left:  this.x -= 1;
        }

        this.direction = nextDirection;
    }
}
