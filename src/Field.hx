import hxd.Pixels;

class Field {
    public var px: Pixels;
    var ant: Ant;

#if (mode == "box")
    var colors: Array<Int> =     [0xFFFFFFFF, 0xFFFF0000, 0xFF02FF01, 0xFF01ffff, 0xFFfdff04, 0xFFfa04fb, 0xFF555555, 0xFF530101, 0xFF015501, 0xFF180cff];
    var direction: Array<Bool> = [true, true, true, false, true, false, false, true, false, true];
#elseif (mode == "triangle")
    var colors: Array<Int> = [0xFFFFFFFF, 0xFFFF0000, 0xFF01ff02, 0xFF04feff, 0xFFfffd01, 0xFFff02fe, 0xFF555555, 0xFF560000, 0xFF005602, 0xFF100fff, 0xFF545601, 0xFF540055];
    var direction:  Array<Bool> = [true, true, true, false, false, false, true, false, false, false, true, true];
#else
    var colors: Array<Int> = [0xFFFFFFFF, 0xFF000000];
    var direction:  Array<Bool> = [true, false];
#end

    public function new(pixels: Pixels, ant: Ant) {
        if (colors.length != direction.length) throw "You bumblefuck did not do your arrays correctly :deccAngery:";
        this.px = pixels;
        this.ant = ant;
    }

    public function adjustWrapping() {
        if (ant.x < 0) { ant.x = px.width - 1; }
        if (ant.y < 0) { ant.y = px.height - 1; }
        if (ant.x >= px.width) { ant.x = 0; }
        if (ant.y >= px.height) { ant.y = 0; }
    }

    public function move() {
        var cpx = px.getPixel(ant.x, ant.y);
        var indx = colors.indexOf(cpx);

        indx++;
        if (indx >= colors.length) indx = 0;
        px.setPixel(ant.x, ant.y, colors[indx]);

        ant.move(direction[indx]);

        adjustWrapping();
    }
}